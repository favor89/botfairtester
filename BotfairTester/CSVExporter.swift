//
//  CSVExporter.swift
//  Botfair
//
//  Created by Wojciech Roszkowiak on 08/03/16.
//  Copyright © 2016 Proud Studios. All rights reserved.
//

import Cocoa

enum ExportType: Int {
    case EventsRunnersBacksSeparately
    case EventsRunnersSeparately
    case EventsSeperately
    case EventsTogether

}

class CSVExporter: NSObject {
    
    func exportContentToFile(content: String, url: NSURL){
        do {
            try content.writeToURL(url, atomically: true, encoding: NSUTF8StringEncoding)
            print("File created at \(url) directory")
        } catch {
            
            print("Failed to create file at \(url) directory")
            print("\(error)")
        }
    }
    
    func exportRunnersSeparately(eventDateString: String, runnerInfo: RunnerInfo){
        let eventFileTitle = "\(eventDateString)-\(runnerInfo.name!)-all.csv"
        var eventFileContent = "timeStamp;price;marketSize;backPrice;backMarketSize;\n";
        let runnerInfosCount = min(runnerInfo.backTimeStamps!.count, runnerInfo.timeStamps!.count);
        for i in 0...runnerInfosCount-1 {
            let line = "\(runnerInfo.timeStamps![i]);\(runnerInfo.prices![i]);\(runnerInfo.marketSizes![i]);\(runnerInfo.backPrices![i]);\(runnerInfo.backMarketSizes![i])\n"
            eventFileContent += line;
        }
        let url = NSURL(fileURLWithPath: self.desktopPath()).URLByAppendingPathComponent(eventFileTitle)
        self.exportContentToFile(eventFileContent, url: url)
    }
    
    func exportRunnersBackSeparately(eventDateString: String, runnerInfo: RunnerInfo, isBack: Bool) {
        let eventFileTitle = isBack ? "\(eventDateString)-\(runnerInfo.name!)-back.csv" : "\(eventDateString)-\(runnerInfo.name!).csv"
        var eventFileContent = "timeStamp;price;marketSize\n";
        let runnerInfosCount = isBack ? runnerInfo.backTimeStamps!.count : runnerInfo.timeStamps!.count;
        for i in 0...runnerInfosCount-1 {
            let line = isBack ? ("\(runnerInfo.backTimeStamps![i]);\(runnerInfo.backPrices![i]);\(runnerInfo.backMarketSizes![i])\n") : ("\(runnerInfo.timeStamps![i]);\(runnerInfo.prices![i]);\(runnerInfo.marketSizes![i])\n")
            eventFileContent += line;
        }
        
        let url = NSURL(fileURLWithPath: self.desktopPath()).URLByAppendingPathComponent(eventFileTitle)
        self.exportContentToFile(eventFileContent, url: url)
    }

    func exportEventsSeparately(eventDateString: String, runnerInfos: [RunnerInfo]) {
        var firstRunnerInfo: RunnerInfo?
        var secondRunnerInfo: RunnerInfo?
        var drawRunnerInfo: RunnerInfo?
        
        for runnerInfo: RunnerInfo in runnerInfos{
            if runnerInfo.name == "The Draw" {
                drawRunnerInfo = runnerInfo
            } else {
                if let _ = firstRunnerInfo {
                    secondRunnerInfo = runnerInfo
                } else {
                    firstRunnerInfo = runnerInfo
                }
            }
        }
        let eventFileTitle = "\(eventDateString)-\(firstRunnerInfo!.name!)-\(secondRunnerInfo!.name!).csv"
        var eventFileContent = "timeStamp;firstTeamPrice;firstTeamMarketSize;firstTeamBackPrice;firstTeamBackMarketSize;secondTeamPrice;secondTeamMarketSize;secondTeamBackPrice;secondTeamBackMarketSize;drawPrice;drawMarketSize;drawBackPrice;drawBackMarkewtSize\n";

        let rowsCount = min(firstRunnerInfo!.timeStamps!.count, firstRunnerInfo!.backTimeStamps!.count, secondRunnerInfo!.timeStamps!.count, secondRunnerInfo!.backTimeStamps!.count, drawRunnerInfo!.timeStamps!.count, drawRunnerInfo!.backTimeStamps!.count)
        for i in 0...rowsCount-1 {
            let line = "\(firstRunnerInfo!.timeStamps![i]);\(firstRunnerInfo!.prices![i]);\(firstRunnerInfo!.marketSizes![i]);\(firstRunnerInfo!.backPrices![i]);\(firstRunnerInfo!.backMarketSizes![i]);\(secondRunnerInfo!.prices![i]);\(secondRunnerInfo!.marketSizes![i]);\(secondRunnerInfo!.backPrices![i]);\(secondRunnerInfo!.backMarketSizes![i]);\(drawRunnerInfo!.prices![i]);\(drawRunnerInfo!.marketSizes![i]);\(drawRunnerInfo!.backPrices![i]);\(drawRunnerInfo!.backMarketSizes![i])\n"
            eventFileContent += line;
        }
        let url = NSURL(fileURLWithPath: self.desktopPath()).URLByAppendingPathComponent(eventFileTitle)
        self.exportContentToFile(eventFileContent, url: url)
    }
    
    func createDirectory(){
        let path = self.desktopPath()
        let fileManager = NSFileManager.defaultManager()
        do
        {
            try fileManager.createDirectoryAtPath(path, withIntermediateDirectories: true, attributes: nil)
        }
        catch let error as NSError
        {
            print("Error while creating a folder. \(error.description)")
        }
        
    }
    
    func exportPrices(exportType: ExportType){
        self.createDirectory()
        for eventInfo: EventInfo in self.getEventInfos() {
            if eventInfo.runnerInfos!.count == 3{
                if exportType == .EventsSeperately {
                    self.exportEventsSeparately(eventInfo.dateString, runnerInfos: eventInfo.runnerInfos!)
                } else {
                    for runnerInfo: RunnerInfo in eventInfo.runnerInfos!{
                        switch exportType{
                        case .EventsRunnersBacksSeparately:
                            self.exportRunnersBackSeparately(eventInfo.dateString, runnerInfo: runnerInfo, isBack: true)
                            self.exportRunnersBackSeparately(eventInfo.dateString, runnerInfo: runnerInfo, isBack: false)
                        case .EventsRunnersSeparately:
                            self.exportRunnersSeparately(eventInfo.dateString, runnerInfo: runnerInfo)
                        default: break
                            
                        }
                    }
                    
                }
            }

        }
    }
    
    func getEventInfos() -> [EventInfo]{
        var eventInfos = [EventInfo]()
        let appDel: AppDelegate = NSApplication.sharedApplication().delegate as! AppDelegate
        let context: NSManagedObjectContext = appDel.managedObjectContext
        let request = NSFetchRequest(entityName: "Event")
//        request.fetchLimit = 5;
        let descriptors = [NSSortDescriptor(key: "id", ascending: true)]
        request.sortDescriptors = descriptors
        let events = (try! context.executeFetchRequest(request)) as! [Event]
        
        for event: Event in events {
            let eventInfo = EventInfo()
            let dateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy HH-mm-ss"
            eventInfo.dateString = dateFormatter.stringFromDate(event.startingDate!)
            let markets = Array(event.markets)
            if markets.count > 0 {
                let firstMarket = markets[0]
                eventInfo.runnerInfos = [RunnerInfo]()
                for runner in firstMarket.runners! {
                    let prices = PriceInfo.pricesForRunner(runner, isBack: false)
                    let backPrices = PriceInfo.pricesForRunner(runner, isBack: true)
                    if prices.count > 180 && backPrices.count > 180 {
                        eventInfo.runnerInfos?.append(self.runnerInfoForRunner(runner))
                    }
                }
                eventInfos.append(eventInfo)
            }

        }
        return eventInfos;
    }

    func desktopPath() -> String{
        return "/Users/\(NSUserName())/Desktop/botfair"
    }
    
    func runnerInfoForRunner(runner: Runner) -> (RunnerInfo){
        print("getting runner info for runner \(runner.name)")
        let runnerInfo = RunnerInfo()
        runnerInfo.timeStamps = [NSTimeInterval]()
        runnerInfo.backTimeStamps = [NSTimeInterval]()
        runnerInfo.prices = [Float]()
        runnerInfo.backPrices = [Float]()
        runnerInfo.marketSizes = [Float]()
        runnerInfo.backMarketSizes = [Float]()
        runnerInfo.name = runner.name
        
        let prices = PriceInfo.pricesForRunner(runner, isBack: false)
        let firstPriceTimpStamp = prices[0].timeStamp!.timeIntervalSince1970;
        for price: PriceInfo in prices {
            let timeStamp = (price.timeStamp?.timeIntervalSince1970)!-firstPriceTimpStamp;
            runnerInfo.timeStamps?.append(timeStamp)
            runnerInfo.prices?.append(price.price)
            runnerInfo.marketSizes?.append(price.marketSize)
        }
        
        let backPrices = PriceInfo.pricesForRunner(runner, isBack: true)
        let firstBackPriceTimpStamp = backPrices[0].timeStamp!.timeIntervalSince1970;
        for backPrice: PriceInfo in backPrices {
            let timeStamp = (backPrice.timeStamp?.timeIntervalSince1970)!-firstBackPriceTimpStamp;
            runnerInfo.backTimeStamps?.append(timeStamp)
            runnerInfo.backPrices?.append(backPrice.price)
            runnerInfo.backMarketSizes?.append(backPrice.marketSize)
        }
        
        return runnerInfo
    }
}
