//
//  ViewController.swift
//  BotfairTester
//
//  Created by Wojciech Roszkowiak on 09/08/16.
//  Copyright © 2016 Handcade. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {
    @IBOutlet weak var infoLabel: NSTextField!
    var viewModel : ViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewModel = ViewModel()
        // Do any additional setup after loading the view.
    }

    override var representedObject: AnyObject? {
        didSet {
        // Update the view, if already loaded.
        }
    }
    @IBAction func loadAllMatches(sender: AnyObject) {
        self.viewModel?.loadAllMatches()
    }


}

