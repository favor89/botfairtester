//
//  Match.swift
//  BotfairTester
//
//  Created by Wojciech Roszkowiak on 09/08/16.
//  Copyright © 2016 Handcade. All rights reserved.
//

import Foundation

class Match : NSObject {
    
    var runners:[Runner]
    var matchName:AnyObject
    
    init(runners:[Runner], matchName:AnyObject) {
        
        self.runners = runners
        self.matchName = matchName
        
        super.init()
        
    }
    
}

