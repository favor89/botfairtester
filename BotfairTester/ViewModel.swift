//
//  ViewModel.swift
//  BotfairTester
//
//  Created by Wojciech Roszkowiak on 09/08/16.
//  Copyright © 2016 Handcade. All rights reserved.
//

import Foundation

class ViewModel: NSObject {
    var matches : [Match]?

    
    
    func desktopPath() -> NSURL{
        return try! NSFileManager().URLForDirectory(.DesktopDirectory, inDomain: .UserDomainMask, appropriateForURL: nil, create: true)
    }
    
    func loadAllMatches() {
            let fileManager = NSFileManager.defaultManager()
            let path = self.desktopPath().path! + "/botfair"

            let enumerator:NSDirectoryEnumerator = fileManager.enumeratorAtPath(path)!
            
            while let element = enumerator.nextObject() as? String {
                let elementPath = path + "/" + element
                var isDirectory: ObjCBool = false
                if NSFileManager.defaultManager().fileExistsAtPath(elementPath, isDirectory: &isDirectory) {
                    if (isDirectory) {
                        var models = [[Match]]()
                        let modelsEnumerator:NSDirectoryEnumerator = fileManager.enumeratorAtPath(elementPath)!
                        var matches = [Match]()
                        while let element = modelsEnumerator.nextObject() as? String {
                            print(element)
                            let csvPath = elementPath + "/" + element
                            do {

                                let csv = try CSV(name: csvPath, delimiter: ";")
                                var runners = [Runner]()
                                for r in 0...2 {
                                    var runnerPrices = [Float]()
                                    var runnerPredictionsDict = [Int : [PricePrediction]]()
                                    
                                    for i in 0...2 {
                                        var pricePredictions = [PricePrediction]()
                                        let column = i*6+r*2
                                        let meanHeader = csv.header[column]
                                        let stdHeader = csv.header[column+1]
                                        for t in 0...csv.rows.count-1 {
                                            let mean = csv.columns[meanHeader]![t]
                                            let std = csv.columns[stdHeader]![t]
                                            let prediction = PricePrediction(mean: Float(mean)!, std: Float(std)!)
                                            pricePredictions.append(prediction)
                                        }
                                        runnerPredictionsDict.updateValue(pricePredictions, forKey: i)
                                    }
                                    let runner = Runner(prices: runnerPrices, pricePredictionsDict: runnerPredictionsDict)
                                    runners.append(runner);
                                }
                                let match = Match(runners: runners, matchName: element)
                                matches.append(match)
                            } catch {
                                    // Catch errors or something
                            }
                        }

                        models.append(matches)
                    }
                }
            }
        
    }
}
