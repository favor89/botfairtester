//
//  Runner.swift
//  BotfairTester
//
//  Created by Wojciech Roszkowiak on 09/08/16.
//  Copyright © 2016 Handcade. All rights reserved.
//

import Foundation

class Runner: NSObject {
    var prices:[Float]
    var pricePredictionsDict:[Int:[PricePrediction]]
    
    init(prices:[Float], pricePredictionsDict:[Int:[PricePrediction]]) {
        
        self.prices = prices
        self.pricePredictionsDict = pricePredictionsDict
        
        super.init()
        
    }
    
    
}
