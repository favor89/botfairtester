//
//  PricePrediction.swift
//  BotfairTester
//
//  Created by Wojciech Roszkowiak on 09/08/16.
//  Copyright © 2016 Handcade. All rights reserved.
//

import Foundation

class PricePrediction : NSObject {
    
    var mean:Float
    var std:Float
    
    init(mean:Float, std:Float) {
        
        self.mean = mean
        self.std = std
        
        super.init()
        
    }
    
}


